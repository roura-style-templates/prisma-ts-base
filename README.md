# Prisma TS Base

## Requirements
  - [Docker](https://docs.docker.com/get-docker/) (Optional)
  - [Yarn](https://yarnpkg.com/) (`npm i -g yarn`)

## Running the server for the first time
  - Clone the repo
  - Run a `yarn install` (And install yarn if you don't have it. This repo relies on the yarn.lock file)
  - Follow it with a `docker-compose up` (Or setup your own DB, up to you)
  - Run a `yarn db-apply`
  - Open VSCode and hit F5 to run the app (Or do a `yarn start` if you want to, I don't care.)

## Running the server
  - Make sure your DB is alive with a `docker-compose up` just in case.
  - Open VSCode and hit F5 to run the app (Or do a `yarn start` if you want to, I don't care.)

## VSCode extensions that'll make you love the project
  - [Prisma](https://marketplace.visualstudio.com/items?itemName=Prisma.prisma)
  - [GraphQL](https://marketplace.visualstudio.com/items?itemName=GraphQL.vscode-graphql)

## Average development resources consumed by the project
  - Node
    - RAM: 199 MiB
    - Disk: 116 KiB/IO
  - Recommended VSCode setup
    - RAM: 546 MiB
    - Disk: 46 MiB/IO

## TODO
  - [ ] GitHooks (Update local DB&types on pull, yarn install on pull, tsc on commit)
  - [ ] Redis implementation for the GraphQL pubsub
