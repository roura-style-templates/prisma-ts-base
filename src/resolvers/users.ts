import { UserWhereUniqueInput, UserCreateInput } from "@prisma/client";
import { Context } from "src/types/definitions";

export const userResolvers = {
  Query: {
    user: async (parent: never, args: UserWhereUniqueInput, context: Context) => context.prisma.user.findOne({
      where: {
        id: args.id,
      },
      include: { polls: true }
    }),
    users: async (parent: never, args: never, context: Context) => context.prisma.user.findMany({
      include: { polls: true }
    })
  },
  Mutation: {
    createUser: async (parent: never, args: UserCreateInput, context: Context) => {
      const result = await context.prisma.user.create({
        data: args,
      })
      context.pubsub.publish('user', {
        user: {
          mutation: 'CREATED',
          data: result
        }
      });
      return result
    },
  },
  Subscription: {
    user: {
      subscribe: (parent: never, args: never, context: Context) => context.pubsub.asyncIterator('user'),
    }
  },
}