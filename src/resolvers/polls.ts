import { PollWhereUniqueInput, PollCreateInput } from "@prisma/client";
import { Context } from "src/types/definitions";

export const pollResolvers = {
  Query: {
    poll: async (parent: never, args: PollWhereUniqueInput, context: Context) => context.prisma.poll.findOne({
      where: {
        id: args.id,
      },
      include: {
        user: true,
        options: true,
        votes: {
          select: { user: true, option: true }
        }
      }
    }),
    polls: async (parent: never, args: never, context: Context) => context.prisma.poll.findMany({
      include: {
        user: true,
        options: true,
        votes: {
          select: { user: true, option: true }
        }
      }
    }),
  },
  Mutation: {
    createPoll: async (parent: never, args: PollCreateInput, context: Context) => context.prisma.poll.create({
      data: {
        description: args.description,
        user: {
          connect: {
            id: args.id
          }
        },
        options: args.options
      }
    })
  },
}