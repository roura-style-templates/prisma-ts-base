import { userResolvers } from './users';
import { pollResolvers } from './polls';

export const resolvers = {
  Query: {
    ...userResolvers.Query,
    ...pollResolvers.Query,
  },
  Mutation: {
    ...userResolvers.Mutation,
    ...pollResolvers.Mutation,
  },
  Subscription: {
    ...userResolvers.Subscription,
  }
}