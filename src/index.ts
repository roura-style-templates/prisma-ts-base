import { PrismaClient } from '@prisma/client';
import { GraphQLServer, PubSub } from 'graphql-yoga'
import { makeExecutableSchema } from '@graphql-tools/schema'

import { resolvers } from 'src/resolvers'
import { typeDefs } from 'src/types';

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
})

const pubsub = new PubSub()
const prisma = new PrismaClient()
const server = new GraphQLServer({
  schema,
  context: (contextParameters) => ({
    prisma,
    pubsub,
    ...contextParameters
  })
})

const options = {
  port: 8000,
  endpoint: '/graphql',
  subscriptions: '/subscriptions',
  playground: '/playground',
}

server.start(options, ({ port }) => console.log(`Server running! 🚀 - http://localhost:${port}/playground`))

process.on('SIGTERM', () => process.exit())
