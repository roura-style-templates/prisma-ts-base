import { PrismaClient } from '@prisma/client';
import { PubSub } from 'graphql-yoga';
import { ContextParameters } from 'graphql-yoga/dist/types';

export interface Context extends ContextParameters {
    prisma: PrismaClient
    pubsub: PubSub
}
