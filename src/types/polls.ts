import gql from 'graphql-tag';

export const pollTypeDefs = gql`
  type Poll {
    id: ID!
    description: String!
    user: User!
    options: [Option!]
    votes: [Vote]
  }

  type Option {
    id: ID!
    text: String!
    poll: Poll!
    votes: [Vote]
  }

  type Vote {
    id: ID!
    user: User!
    poll: Poll!
    option: Option!
  }

  type Query {
    polls: [Poll]
    votes: [Vote]
    poll(id: ID!): Poll
  }

  input CreatePollOptionsInput {
    text: String
  }

  input ConnectPollOptionsInput {
    id: String
  }

  input PollOptionsInput {
    create: [CreatePollOptionsInput]
    connect: [ConnectPollOptionsInput]
  }

  type Mutation {
    createPoll(description: String! id: ID! options: [PollOptionsInput]): Poll
    createVote(userID: ID! pollID: ID! optionID: ID!): Vote
  }
`