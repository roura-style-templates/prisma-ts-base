import { mergeTypeDefs } from "@graphql-tools/merge";
import { pollTypeDefs } from "./polls";
import { userTypeDefs } from "./users";

export const typeDefs = mergeTypeDefs([
  userTypeDefs,
  pollTypeDefs
])