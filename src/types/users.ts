import gql from 'graphql-tag';

export const userTypeDefs = gql`
  type User {
    id: ID!
    name: String!
    dni: String
    polls: [Poll]
  }

  type Query {
    users: [User]
    user(id: ID!): User
  }

  type Mutation {
    createUser(name: String!): User
  }
  
  type UserSubscription {
    mutation: String!
    data: User!
  }

  type Subscription {
    user: UserSubscription!
  }
`
