# Migration `20201101190754-init`

This migration has been generated by Franco Roura at 11/1/2020, 4:07:54 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
PRIMARY KEY ("id")
)

CREATE TABLE "Poll" (
    "id" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE,
PRIMARY KEY ("id")
)

CREATE TABLE "Option" (
    "id" TEXT NOT NULL,
    "text" TEXT NOT NULL,
    "pollId" TEXT NOT NULL,

    FOREIGN KEY ("pollId") REFERENCES "Poll"("id") ON DELETE CASCADE ON UPDATE CASCADE,
PRIMARY KEY ("id")
)

CREATE TABLE "Vote" (
    "id" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "pollId" TEXT NOT NULL,
    "optionId" TEXT NOT NULL,

    FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("pollId") REFERENCES "Poll"("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("optionId") REFERENCES "Option"("id") ON DELETE CASCADE ON UPDATE CASCADE,
PRIMARY KEY ("id")
)
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration ..20201101190754-init
--- datamodel.dml
+++ datamodel.dml
@@ -1,0 +1,37 @@
+datasource db {
+  provider = "sqlite"
+  url = "***"
+}
+
+generator client {
+  provider = "prisma-client-js"
+}
+
+model User {
+  id          String @default(cuid()) @id
+  name        String
+  polls       Poll[]
+}
+
+model Poll {
+  id          String  @default(cuid()) @id
+  description String
+  user        User
+  options     Option[]
+  votes       Vote[]
+
+}
+
+model Option {
+  id          String  @default(cuid()) @id
+  text        String
+  poll        Poll
+  votes       Vote[]
+}
+
+model Vote {
+  id          String  @default(cuid()) @id
+  user        User
+  poll        Poll
+  option      Option
+}
```


