const path = require('path');
const webpack = require('webpack')
const nodeExternals = require("webpack-node-externals");

module.exports = {
  mode: 'production',
  entry: './src/index.ts',
  target: 'node',
  externals: [nodeExternals()],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
  },
  resolve: {
    extensions: ['*', '.mjs', '.js', '.ts', '.vue', '.json', '.gql', '.graphql'],
    modules: ['src', 'node_modules'],
    alias: {
      src: path.resolve(__dirname, 'src')
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?/,
        use: 'ts-loader',
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: '@graphql-tools/webpack-loader',
      }
    ]
  }
};